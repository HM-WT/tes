var express = require('express');
var router = express.Router();

var User = require('../model/user');

router.get('/', function(req, res, next) {
    res.render('regi', 
        {
            title: 'Register',
            msg: ''
        }
    );
});
 
/* POST helo page. */
router.post('/', function(req, res, next) {
    var name = req.body.name;
	var passwd = req.body.passwd;
    res.render('regi', 
        {
            title: 'Register',
            msg: "name:" + name + " pass:" + passwd 
        }
    );

	var regi = new User({
		name:name,
		passwd:passwd
	});

	regi.save(function(err){
		if(err) throw err;

		console.log('success');
	});
});

module.exports = router;
