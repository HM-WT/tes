var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var session = require('express-session');

var Userdata = require('../model/user');

router.get('/', function(req, res, next) {
   	if(!req.session.user){
		res.render('login', 
    	    {
        	    title: 'Login',
            	msg: ''
	        }
	    );
	}else{
		res.render('page', 
    	    {
        	    title: 'Page',
            	msg: 'already'
	        }
	    );
	}		
});
 
/* POST helo page. */
router.post('/', function(req, res, next) {
    var name = req.body.name;
	var passwd = req.body.passwd;
	
//	var data = mongoose.model('user',User);
	
	Userdata.find({
		name : name,
		passwd: passwd
	},function(err, result){
		if(err) throw err;

		if(result == ""){
			res.render('login',{
		   	 	title: 'Login',
	    	    msg: "no"
			});
		}else{
			req.session.user = name;
			console.log(req.session.user);
			res.redirect('http://localhost:3000');
		}
	});
});


module.exports = router;
